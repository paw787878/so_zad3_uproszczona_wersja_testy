#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

void error(int nr) {
    printf("\n\n\nERROR %d\n\n\n", nr);
    fflush(stdout);
    exit(1);
}

int main(int argc, char** argv) {
    // Proces A sie wykonuje
    int B_pid;
    pid_t C_pid;
    pid_t A_pid = getpid();
    int fork_code_2;

    int fork_code = fork();

    switch (fork_code) {
        case -1:
            perror("fork");
            exit(1);
        case 0:
            // proces B sie wykonuje
            B_pid = getpid();

            fork_code_2 = fork();

            switch (fork_code_2) {
                case -1:
                    perror("fork");
                    exit(1);
                case 0:
                    // Proces C

                    C_pid = getpid();
                    if (changeparent() != 0) {
                        error(453);
                    }

                    if (getppid() != A_pid) {
                        printf("\n\n\nERROR 1\n\n\n");
                        fflush(stdout);
                        exit(1);
                    }

                    if (getoppid(C_pid) != B_pid) {
                        printf("\n\n\nERROR 2\n\n\n");
                        fflush(stdout);
                        exit(1);
                    }

                    break;
                default:
                    // proces B spi 2 sec
                    sleep(2);
            }
            break;
        default:
            // A czeka
            wait(NULL);
            break;
    }
    fflush(stdout);
    return 0;
}