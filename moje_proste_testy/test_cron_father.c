
#include <assert.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

void error(int nr) {
    printf("\n\n\nERROR %d\n\n\n", nr);
    fflush(stdout);
    exit(1);
}

int main(int argc, char** argv) {
    // Proces A sie wykonuje
    pid_t A_pid = getpid();

    if (getoppid(165) != 108) {
        printf("dal %d, a mial dac %d\n", getoppid(165), 108);
        error(1);
    }
    fflush(stdout);
    return 0;
}
