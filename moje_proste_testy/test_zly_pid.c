#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <errno.h>

void error(int nr) {
    printf("\n\n\nERROR %d\n\n\n", nr);
    fflush(stdout);
    exit(1);
}

int main(int argc, char** argv) {
    // Proces A sie wykonuje
    pid_t A_pid = getpid();


    if (getoppid(66666) != -1)
        error(1);
    if (errno != EINVAL)
        error(2);
    fflush(stdout);
    return 0;
}