#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

void error(int nr) {
    printf("\n\n\nERROR %d\n\n\n", nr);
    fflush(stdout);
    exit(1);
}

int main(int argc, char** argv) {
    // Proces A sie wykonuje
    int B_pid;
    pid_t C_pid;
    pid_t A_pid = getpid();
    int fork_code_2;

    int fork_code = fork();

    switch (fork_code) {
        case -1:
            perror("fork");
            exit(1);
        case 0:
            // proces B sie wykonuje
            B_pid = getpid();

            sleep(1);

            if (getoppid(B_pid) != A_pid)
                error(555);

            break;
        default:
            // A nie czeka, tylko osieraca
            break;
    }
    fflush(stdout);
    return 0;
}